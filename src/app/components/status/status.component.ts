import { Component, Input } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StatusService } from '../../service/statusService/status.service';
import { IStateGetStatus, IStatus } from '../../Interface/IStatus';
import { sortCase, sortCaseFilter } from '../../utils/jobSort';
import { limit } from '../../constantCommon/constantCommon';
@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrl: './status.component.css'
})
export class StatusComponent {
  constructor(
    private statusService: StatusService,
    private notification: NzNotificationService
  ) { }

  modalItemDelete: IStatus = {
    idStatus: 0,
    default: false,
    nameStatus: "",
    description: "",
  };
  modalItemAddorUpdate = {
    listData: {} as IStatus[],
    itemStatus: {} as IStatus,
    index: 0 as number | undefined,
  }
  modalStatus = {
    modalAddorUpdate: false,
    modalDelete: false,
  };

  handleOpenModalAddorUpdate(itemStatus?: IStatus, index?: number) {
    if (itemStatus) {
      this.modalItemAddorUpdate.index = index;
      this.modalItemAddorUpdate.itemStatus = itemStatus
      this.modalItemAddorUpdate.listData = this.listData
      this.statusService.checkIdStatus(this.modalItemAddorUpdate.itemStatus.idStatus).subscribe({
        next: (v) => {
          if (v.status === true) {
            this.modalStatus.modalAddorUpdate = true
          }
          else {
            this.notification.create('warning', 'Trạng thái không tồn tại', '');
            this.modalStatus.modalAddorUpdate = false;
            this.handleGet(this.stateGetStatus);
          }
        },
        error: (e) => {
          this.notification.create('error', `${e.message}`, '');
          this.modalStatus.modalAddorUpdate = false;
        }
      }
      );

    } else {
      this.modalItemAddorUpdate.itemStatus = {
        idStatus: 0,
        nameStatus: '',
        description: '',
        default: false
      };
      this.modalStatus.modalAddorUpdate = true
    }
  }
  handleUpdateTable(data: any) {
    this.listData = data
  }
  handleCloseModalAddorUpdate() {
    this.modalStatus.modalAddorUpdate = false;
  }

  handleOpenModalDelete(itemStatus: IStatus) {
    console.log(itemStatus.idStatus, "itemStatus");
    this.statusService.checkStatus(itemStatus.idStatus).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
          this.modalStatus.modalDelete = false;
          this.handleGet(this.stateGetStatus);
        }
        else {
          this.modalItemDelete = itemStatus;
          this.modalStatus.modalDelete = true;
        }
        this.modalItemDelete = itemStatus;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
      },
    });
  }

  handleCloseModalDelete() {
    this.modalStatus.modalDelete = false;
  }

  stateGetStatus = {
    textSearch: '',
    sortType: 'ASC',
    sortData: 'id',
    currentPage: 1,
  };
  currentPage = 1
  calcu = (this.currentPage - 1) * 5 + 1
  isSearch: boolean = false;
  listData: IStatus[] = [];
  totalRecord: number = 0;
  isLoading = false;
  sortCase = sortCase;
  sortItem: number = 1;
  limit = limit;
  inputValue() {
    this.stateGetStatus.textSearch = ""
  }
  ngOnInit(): void {
    this.handleGet(this.stateGetStatus);
  }
  // get status
  handleGet(stateStatus: any) {
    this.isLoading = true;
    this.stateGetStatus = stateStatus;
    this.statusService.get(this.stateGetStatus).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;

      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });

  }
  // pagination status
  handlePagination(event: any): void {
    if (!this.isSearch) {
      this.stateGetStatus.textSearch = '';

      this.isLoading = true;
      this.stateGetStatus.currentPage = event;
      this.statusService.get(this.stateGetStatus).subscribe({
        next: (v) => {
          this.listData = v.content.list;
          this.totalRecord = v.content.totalRecord;
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    } else {
      this.isLoading = true;
      this.stateGetStatus.currentPage = event;
      this.statusService.get(this.stateGetStatus).subscribe({
        next: (v) => {
          this.listData = v.content.list;
          this.totalRecord = v.content.totalRecord;
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    }
  }
  // search status
  handleSearch(event: IStateGetStatus) {
    console.log(event);

    this.isSearch = true;
    this.stateGetStatus.currentPage = 1
    this.statusService.get(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }
  // sort status
  handleSelect(event: IStateGetStatus): void {
    this.stateGetStatus.currentPage = 1;
    this.statusService.get(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }

}
