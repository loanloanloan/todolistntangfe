import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StatusService } from '../../../../service/statusService/status.service';
import { IStatus } from '../../../../Interface/IStatus';

@Component({
  selector: 'app-modal-add-or-update-status',
  templateUrl: './modal-add-or-update-status.component.html',
  styleUrl: './modal-add-or-update-status.component.css'
})
export class ModalAddOrUpdateStatusComponent {
  // closeModal: any;
  constructor(
    private statusService: StatusService,
    private notification: NzNotificationService
  ) { }
  //
  @Input() isVisibleStatus: boolean = false;
  @Input() item: {
    listData: IStatus[], itemStatus: IStatus, index: number | undefined
  } = {
      listData: [],
      itemStatus: {
        idStatus: 0,
        nameStatus: '',
        description: '',
        default: false,
      },
      index: 0
    }
  @Input() stateStatus: any = {
    textSearch: '',
    sortType: '',
    sortData: '',
    currentPage: 1,
  };
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  @Output() reselectSort: EventEmitter<number> = new EventEmitter<number>();
  @Output() isAddOrUpdate = new EventEmitter<any>();
  @Output() getStatus = new EventEmitter<any>();
  @Output() sortItem = new EventEmitter<any>();
  @Output() listDataUp = new EventEmitter<any>()


  isLoading = false;
  showValidate: boolean = false;
  idStatus?: number = 0;
  nameStatus: string = '';
  description: string = '';
  default?: boolean = false;

  stateSortCase = {
    sortType: 'ASC',
    sortData: 'id',
  };

  handleResetState(): void {
    this.idStatus = 0;
    this.nameStatus = '';
    this.description = '';
    this.default = false;
  }

  onInput() {
    this.showValidate = this.nameStatus.trim() === '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['itemStatus'] &&
      changes['itemStatus'].currentValue.idStatus > 0
    ) {
      this.idStatus = this.item.itemStatus.idStatus;
      this.nameStatus = this.item.itemStatus.nameStatus;
      this.description = this.item.itemStatus.description;
      this.default = this.item.itemStatus.default;
    } else if (this.item.itemStatus.idStatus > 0) {
      this.idStatus = this.item.itemStatus.idStatus;
      this.nameStatus = this.item.itemStatus.nameStatus;
      this.description = this.item.itemStatus.description;
      this.default = this.item.itemStatus.default;
    } else {
      this.idStatus = undefined;
      this.nameStatus = '';
      this.description = '';
      this.default = false;
    }
  }

  handleOk(): void {
    if (this.nameStatus) {
      this.isLoading = true;
      if (this.item.itemStatus.idStatus) {
        this.isLoading =true;
        this.statusService
          .update(this.idStatus, this.nameStatus, this.description)
          .subscribe({
            next: (v) => {
              if (v.status === false) {
                this.notification.create('error', `${v.message}`, '');
                if (v.content.warning == null) {
                  console.log('warning');
                } else {
                  this.notification.create(
                    'warning',
                    `${v.content.warning}`,
                    ''
                  );
                }
              } else {
                this.notification.create('success', `${v.message}`, '');
                const updatedListData = [...this.item.listData]
                const itemStatusUpdate: IStatus = {
                  idStatus: this.item.itemStatus.idStatus,
                  nameStatus: this.nameStatus,
                  description: this.description,
                  default: this.item.itemStatus.default,
                }
                if (this.item.index === undefined) {
                  updatedListData[0] = itemStatusUpdate
                } else {
                  updatedListData[this.item.index] = itemStatusUpdate
                }
                this.listDataUp.emit(updatedListData)
                console.log(this.item.index)
              }
              this.isLoading = false;
              this.closeModal.emit();
              this.handleResetState();
            },
            error: (error) => {
              this.notification.create('error', `${error.message}`, '');
              this.isLoading = false;
            },
          });
      } else {
        this.isLoading =true;
        this.statusService.add(this.nameStatus, this.description).subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create('warning', `${v.message}`, '');
            } else {
              this.notification.create('success', `${v.message}`, '');
            } const currentPage = v.content.currentPage;
            this.stateStatus.textSearch = ""
            this.stateStatus.sortData = "id"
            this.stateStatus.sortType = "ASC"
            this.stateStatus.currentPage = currentPage
            this.isLoading = false;
            this.closeModal.emit();
            this.reselectSort.emit(1)
            this.getStatus.emit(this.stateStatus);
            this.handleResetState();
          },
          error: (error) => {
            this.notification.create('error', `${error.message}`, '');
            this.isLoading = false;
          },
        });
      }
    } else {
      this.showValidate = false;
    }
  }


  handleCancel(): void {
    // console.log('Button cancel clicked!');
    this.closeModal.emit();
    this.handleResetState();
    this.showValidate = false;
  }
}
