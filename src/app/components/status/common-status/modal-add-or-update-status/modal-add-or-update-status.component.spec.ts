import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddOrUpdateStatusComponent } from './modal-add-or-update-status.component';

describe('ModalAddOrUpdateStatusComponent', () => {
  let component: ModalAddOrUpdateStatusComponent;
  let fixture: ComponentFixture<ModalAddOrUpdateStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalAddOrUpdateStatusComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalAddOrUpdateStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
