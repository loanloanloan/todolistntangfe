import { Component, EventEmitter, Input, Output } from '@angular/core';
import { sortCase,sortCaseFilter } from '../../../../utils/statusSort';
import { IStateGetStatus } from '../../../../Interface/IStatus';

@Component({
  selector: 'app-header-status',
  templateUrl: './header-status.component.html',
  styleUrl: './header-status.component.css'
})
export class HeaderStatusComponent {
  sortCase = sortCase;
  sortItem: number = 1;
  stateStatus: IStateGetStatus = {
    textSearch: '',
    sortType: 'ASC',
    sortData: 'id',
    currentPage: 1,
  };
  @Output() searchStatus = new EventEmitter<IStateGetStatus>();
  @Output() sortStatus = new EventEmitter<IStateGetStatus>();
  @Output() openModalAdd: EventEmitter<void> = new EventEmitter<void>();
  
  handleOpenModalAdd(){
    this.openModalAdd.emit()
  }

  inputValue() {
    this.stateStatus.textSearch = '';
    this.searchStatus.emit(this.stateStatus);
  }
  handleSearch() {
    this.searchStatus.emit(this.stateStatus);
    
  }
  
  handleSelect(e: number): void {
    const data = sortCaseFilter(e);
    if (typeof data === 'number') {
      return;
    }
    this.stateStatus.sortType = data.sortType;
    this.stateStatus.sortData = data.sortData;
    this.stateStatus.currentPage = 1;
    this.sortStatus.emit(this.stateStatus);

    // test sort changes
    // console.log(e,'eeeeee');
    // console.log(this.stateStatus.sortType,'this.stateStatus.sortType');
    // console.log(this.stateStatus.sortData,'this.stateStatus.sortData');

    // this.statusService.get(this.stateGetStatus).subscribe({
    //   next: (v) => {
    //     this.listData = v.content.list;
    //     this.totalRecord = v.content.totalRecord;
    //     if (v.status === false) {
    //       this.notification.create('warning', `${v.message}`, '');
    //     }
    //     this.isLoading = false;
    //   },
    //   error: (error) => {
    //     this.notification.create('error', `${error.message}`, '');
    //     this.isLoading = false;
    //   },
    // });
  }
}
