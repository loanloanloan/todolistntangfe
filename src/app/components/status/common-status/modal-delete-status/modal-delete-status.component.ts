import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IStatus } from '../../../../Interface/IStatus';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StatusService } from '../../../../service/statusService/status.service';

@Component({
  selector: 'app-modal-delete-status',
  templateUrl: './modal-delete-status.component.html',
  styleUrl: './modal-delete-status.component.css'
})
export class ModalDeleteStatusComponent {
  constructor(
    private statusService: StatusService,
    private notification: NzNotificationService
  ) {}

  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  @Output() getStatus = new EventEmitter<any>();
  @Input() isVisibleDeleteStatus: boolean = false;
  @Input() itemStatus: IStatus = {
    idStatus: 0,
    nameStatus: '',
    description: '',
    default: false,
  };
  @Input() stateStatus: any = {
    textSearch: '',
    sortType: '',
    sortData: '',
    currentPage: 1,
  };
  isLoading = false;

  // showModal(): void {
  //   this.isVisible = true;
  // }

  handleOk(): void {
    this.isLoading = true;
    //hàm check status
    this.statusService.checkStatus(this.itemStatus.idStatus).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
          // this.handleGet(this.stateGetStatus);
       
          this.closeModal.emit();
        }
        //hàm xóa
        this.statusService
        .delete(this.itemStatus.idStatus, this.stateStatus.currentPage)
        .subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create('error', `${v.message}`, '');
              this.closeModal.emit();
              if (v.content.warning == null) {
                console.log('warning');
              } else {
                this.notification.create(
                  'warning',
                  `${v.content.warning}`,
                  ''
                );
              }
            } else {
              this.notification.create('success', `${v.message}`, '');
            }
            this.isLoading = false;
            this.closeModal.emit();
            this.getStatus.emit(this.stateStatus);
          },
          error: (error) => {
            this.notification.create('error', `${error.message}`, '');
            this.isLoading = false;
          },
        });
    
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
      },
    });
  }

  handleCancel(): void {
    // console.log('Button cancel clicked!');
    this.closeModal.emit();
  }
}
