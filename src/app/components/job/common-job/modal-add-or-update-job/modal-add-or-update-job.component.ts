import { Component, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
// import { FormGroup, FormControl } from '@angular/forms';
import { IPayloadJob, IStateGetJob } from '../../../../Interface/IJob';
import { IJob } from '../../../../Interface/IJob';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StatusService } from '../../../../service/statusService/status.service';
import { JobService } from '../../../../service/jobService/job.service';

@Component({
  selector: 'app-modal-add-or-update-job',
  templateUrl: './modal-add-or-update-job.component.html',
  styleUrl: './modal-add-or-update-job.component.css'
})
export class ModalAddOrUpdateJobComponent {

  @Input() isVisible: boolean = false;

  @Input() totalItem: number = 0;

  stateGetPayload: IPayloadJob = {
    idJob: undefined,
    nameJob: '',
    idStatus: 1,
    statusName: '',
    idManage: 0,
    priority: 'ONE'
  }

  @Input() stateGet: IStateGetJob = {
    textSearch: '',
    sortData: '',
    sortType: '',
    currentPage: 1
  };

  @Input() item: {
    listData: IJob[], itemData: IJob, index: 0
  } = {
      listData: [],
      itemData: {
        idJob: 0,
        nameJob: '',
        idStatus: 0,
        nameStatus: '',
        priority: 'ONE',
        progress: '0',
        progressDouble: 0,
        hasChild: false,
        createdAt: new Date(),
        updatedAt: new Date(),
        idManage: 0
      },
      index: 0
    }

  @Output() closeModalJob: EventEmitter<void> = new EventEmitter<void>();
  @Output() reselectSort: EventEmitter<number> = new EventEmitter<number>();

  isLoadingSelect: boolean = false
  isLoadingConfirm: boolean = false
  showValidate: boolean = false;
  options: any = [];

  title: string = '';
  idJob?: number = 0;
  nameJob: string = '';
  priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE' = 'ONE';
  idStatus: number = 1;
  nameStatus: string | undefined = ''
  idManage?: number;

  // jobForm = new FormGroup({
  //   jobName: new FormControl(''),
  //   priority: new FormControl(null),
  //   status: new FormControl(null)
  // });

  constructor(private statusService: StatusService,
    private notification: NzNotificationService,
    private jobService: JobService) { }

  onInput() {
    this.showValidate = this.nameJob.trim() === '';
  }

  handleResetState() {
    this.idJob = 0;
    this.nameJob = '';
    this.priority = 'ONE';
    this.idStatus = 1;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['item'] && changes['item'].currentValue.itemData.idJob > 0) {
      //   this.title = "Cập nhật công việc";
      //   this.idJob = this.item.itemData.idJob;
      //   this.nameJob = this.item.itemData.nameJob;
      //   this.priority = this.item.itemData.priority;
      //   this.idStatus = this.item.itemData.idStatus;
      // } else if (this.item.itemData.idJob > 0) {
      //   this.title = "Cập nhật công việc";
      //   this.idJob = this.item.itemData.idJob;
      //   this.nameJob = this.item.itemData.nameJob;
      //   this.priority = this.item.itemData.priority;
      //   this.idStatus = this.item.itemData.idStatus;
    } else {
      this.title = "Thêm công việc";
      this.idJob = undefined;
      this.nameJob = '';
      this.priority = 'ONE';
      this.idStatus = 1;
      this.idManage = 0;
    }
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(data: IPayloadJob): void {
    this.isLoadingConfirm = true;
    this.jobService.add(data).subscribe({
      next: (v) => {
        this.reselectSort.emit(9)
        this.stateGet.currentPage = 1,
          this.stateGet.sortData = 'updatedAt',
          this.stateGet.sortType = 'desc',
          this.stateGet.textSearch = ''
        this.jobService.refreshListData(this.stateGet).subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create("warning", `${v.message}`, '');
            }
          },
          error: (error) => {
            this.notification.create(
              "error", `${error.message}`, '');
          }
        })
        if (v.status === false) {
          if (v.content.warning == "") {
            this.notification.create(
              "error", `${v.message}`, '');
          } else {
            this.notification.create(
              "error", `${v.message}`, '');
            this.notification.create(
              "warning", `${v.content.warning}`, '');
          }
        } else {
          if (v.content.warning == "") {
            this.notification.create(
              "success", `${v.message}`, '');
          } else {
            this.notification.create(
              "warning", `${v.content.warning}`, '');
            this.notification.create(
              "success", `${v.message}`, '');
          }
        }
        this.isLoadingConfirm = false;
        this.handleResetState();
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
        this.isLoadingConfirm = false
        this.closeModalJob.emit();
      }
    })
    this.showValidate = true;
    this.closeModalJob.emit();
  }

  onSearch() {
    this.isLoadingSelect = true;
    this.statusService.getAll().subscribe({
      next: (v) => {
        this.options = v.content.list.map((item: any) => ({
          label: item.nameStatus,
          value: item.idStatus,
        }));
        this.isLoadingSelect = false
      }, error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
        this.isLoadingSelect = false
      }
    }
    );
  }

  handleCancel() {
    console.log("Linh")
    this.closeModalJob.emit();
    this.handleResetState();
    this.showValidate = false;
  }

  handleUpdate(changes: SimpleChanges) {
    if(changes['item'] && changes['item'].currentValue.itemData.idJob > 0){
      this.title = 'Sua cong viec';
      this.idJob = this.item.itemData.idJob;
      this.nameJob = this.item.itemData.nameJob;
      this.priority = this.item.itemData.priority;
      this.idStatus = this.item.itemData.idStatus;
    }else if(changes['item'] && changes['item'].currentValue.itemData.idJob > 0){
      this.title = 'Sua cong viec';
      this.idJob = this.item.itemData.idJob;
      this.nameJob = this.item.itemData.nameJob;
      this.priority = this.item.itemData.priority;
      this.idStatus = this.item.itemData.idStatus;
    }
  }


}


