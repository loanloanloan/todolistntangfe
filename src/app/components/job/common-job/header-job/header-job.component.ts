import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { sortCase, sortCaseFilter } from '../../../../utils/jobSort';
import { IJob, IStateGetJob } from '../../../../Interface/IJob';
import { JobService } from '../../../../service/jobService/job.service';

@Component({
  selector: 'app-header-job',
  templateUrl: './header-job.component.html',
  styleUrl: './header-job.component.css'
})
export class HeaderJobComponent {
  constructor(private jobService: JobService) { }

  @Output() searchJob = new EventEmitter<IStateGetJob>;
  @Output() openModalAdd: EventEmitter<void> = new EventEmitter<void>();


  sortCase = sortCase
  sortItem = 9

  stateGet = {
    textSearch: '', 
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }
  
  inputValue() {
    this.stateGet.textSearch = "";
    this.searchJob.emit(this.stateGet);
  }

  handleOpenModalAdd() {
    this.openModalAdd.emit();
  }

  //sort Job
  handleSelectData(e: number) {
    const data = sortCaseFilter(e)
    if (typeof (data) === 'number') {
      return
    }
    this.stateGet.sortType = data.sortType
    this.stateGet.sortData = data.sortData
    this.stateGet.currentPage = 1
    this.jobService.refreshListData(this.stateGet).subscribe();
  }

  handleSearch() {  
    this.searchJob.emit(this.stateGet);
  }

}
