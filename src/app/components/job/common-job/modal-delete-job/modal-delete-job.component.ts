import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IStateGetJob } from '../../../../Interface/IJob';
import { IJob } from '../../../../Interface/IJob';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { JobService } from '../../../../service/jobService/job.service';

@Component({
  selector: 'app-modal-delete-job',
  templateUrl: './modal-delete-job.component.html',
  styleUrl: './modal-delete-job.component.css'
})
export class ModalDeleteJobComponent {
  @Input() isVisibleDelete: boolean = false;

  @Input() stateGet: IStateGetJob = {
    textSearch: '',
    sortData: '',
    sortType: '',
    currentPage: 1
  };

  @Input() item: IJob = {
    idJob: 0,
    nameJob: '',
    idStatus: 0,
    nameStatus: '',
    priority: 'ONE',
    progress: '0',
    progressDouble: 0,
    hasChild: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    idManage: 0
  };
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  isLoadingDelete: boolean = false;
  constructor( private notification: NzNotificationService,
    private jobService: JobService) {}

  // showModal(): void {
  //   this.isVisibleDelete = true;
  // }

  handleOk(): void {
    this.isLoadingDelete = true;
    this.jobService.delete(this.item.idJob, this.stateGet.currentPage, this.item.idManage, 5).subscribe({
      next: (v) => {
        if (v.status == false) {
          this.notification.create('error', `${v.message}`, '');
          this.notification.create('warning', `${v.content.warning}`, '');
        } else {
          this.notification.create('success', `${v.message}`, '');
        }
        this.isLoadingDelete = false;
        this.stateGet.currentPage = v.content.currentPage;

        this.closeModal.emit();
        this.jobService.refreshListData(this.stateGet).subscribe()
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoadingDelete = false;
      },
    });
    this.closeModal.emit();
  }

  handleCancel(): void {
    this.closeModal.emit();
  }
}
