import { Component, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { IJob, IStateGetJob } from '../../../Interface/IJob';
import { takeUntil, Subject } from 'rxjs';
import { sortCase, sortCaseFilter } from '../../../utils/jobSort';
import { JobService } from '../../../service/jobService/job.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { priorityFormat } from '../../../utils/priorityFormat';
import { limit } from '../../../constantCommon/constantCommon';
@Component({
  selector: 'app-modal-job',
  templateUrl: './modal-job.component.html',
  styleUrl: './modal-job.component.css'
})
export class ModalJobComponent {
  openModalAdd: any;
  constructor(private jobService: JobService, private notification: NzNotificationService) { }
  private destroyed$ = new Subject()

  isVisible = false;
  isLoadingSelect: boolean = false
  isLoadingConfirm: boolean = false
  showValidate: boolean = false;
  options: any = [];

  title: string = '';
  idJob?: number = 0;
  nameJob: string = '';
  priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE' = 'ONE';
  idStatus: number = 1;
  nameStatus: string | undefined = ''
  idManage?: number = 0;

  @Input() isVisibleModal: boolean = false
  @Input() item: IJob = {
    idJob: 0,
    nameJob: '',
    idStatus: 0,
    nameStatus: '',
    priority: 'ONE',
    progress: '0',
    progressDouble: 0,
    hasChild: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    idManage: 0
  }
  @Input() isLoading: boolean = false

  @Input() stateGet = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }
  @Output() searchJob = new EventEmitter<IStateGetJob>;
  // @Output() openModalAdd: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();

  sortCase = sortCase
  sortItem = 9
  totalRecord = 0;

  isSearch: boolean = false;
  limit = limit;
  listData: IJob[] = []

  // open modal
  modalJob = {
    modalJobItem: false,
    modalAddorUpdate: false,
    modalDelete: false
  }

  modalItemAddorUpdate = {
    listData: {} as IJob[] | any,
    index: 0 as number | any,
    itemData: {} as IJob | any
  }

  modalDeleteItem: IJob | any = {}
  modalJobItem: IJob | any = {}
  modalItemDelete: IJob | any = {}



  handleOpenModalAddorUpdate(data?: IJob, index?: number) {
    // console.log(data, "dtada addor update job item");

    if (data) {
      this.modalItemAddorUpdate.listData = this.listData
      this.modalItemAddorUpdate.itemData = data
      this.modalItemAddorUpdate.index = index
      this.modalJob.modalAddorUpdate = true
      this.openModalAdd.emit();
    } else {
      this.modalItemAddorUpdate.itemData = {
        idJob: 0,
        nameJob: '',
        priority: 'ONE',
        nameStatus: '',
      }
      this.modalJob.modalAddorUpdate = true;
    }
    // this.openModalAdd.emit();
  }

  handleCloseModalJob() {
    this.breadcrumbItems = [];
    this.closeModal.emit();
  }

  handleCloseModalAddorUpdate(): void {
    this.modalJob.modalAddorUpdate = false;
  }

  handleOpenModalDelete(data: IJob) {
    if (data) {
      this.modalDeleteItem = data
      this.modalJob.modalDelete = true;
    } else {
      this.modalDeleteItem = {}
    }
    // this.statusService.checkStatus(itemStatus.idStatus).subscribe({
    //   next: (v) => {
    //     if (v.status === false) {
    //       this.notification.create('warning', `${v.message}`, '');
    //       this.modalStatus.modalDelete = false;
    //       this.handleGet(this.stateGetStatus);
    //     }
    //     else {
    //       this.modalItemDelete = itemStatus;
    //       this.modalStatus.modalDelete = true;
    //     }
    //     // this.handleGet(this.stateGetStatus);
    //     this.modalItemDelete = itemStatus;
    //     console.log(this.modalItemDelete.idStatus, "bật modal deleted status");
    //   },
    //   error: (error) => {
    //     this.notification.create('error', `${error.message}`, '');
    //   },
    // });

    ////hàm check job còn tồn tại hay k
    // this.jobService.getJobById(this.stateGet, data.idJob).subscribe({
    //   next: (v) => {
    //     if (v.status === false) {
    //       this.notification.create("warning", `${v.message}`, '');
    //     }
    //     this.modalDeleteItem = data
    //     this.modalJob.modalDelete = true;
    //     this.isLoading = false
    //   },
    //   error: (e) => {
    //     this.notification.create(
    //       "error", `${e.message}`, '');
    //     this.isLoading = false
    //   }
    // })
  }

  handleCloseModalDelete() {
    this.modalDeleteItem = undefined;
    this.modalJob.modalDelete = false;
  }

  //test link

  breadcrumbItems: IJob[] = [];

  goBack() {
    if (this.breadcrumbItems.length === 1) {
      this.breadcrumbItems.pop();
      this.closeModal.emit();
    }
    else {
      this.handleGetById(this.breadcrumbItems[this.breadcrumbItems.length - 2])
      this.breadcrumbItems.pop();
    }
  }
  removeBreadcrumbItemsAfterIndex(item: number) {
    if ( item === this.breadcrumbItems.length - 1 ) {
      console.log("chuẩn");
      this.handleGetById(this.breadcrumbItems[item])
    }
    else {
      console.log("đéo");
      this.handleGetById(this.breadcrumbItems[item])
      this.breadcrumbItems = this.breadcrumbItems.slice(0, item + 1); 
    }
  }
  //hết link

  //reset search
  inputValue() {
    this.stateGet.textSearch = "";
    this.searchJob.emit(this.stateGet);
  }

  //sort Job
  handleSelectData(e: number) {
    const data = sortCaseFilter(e)
    if (typeof (data) === 'number') {
      return
    }
    this.stateGet.sortType = data.sortType
    this.stateGet.sortData = data.sortData
    this.stateGet.currentPage = 1
    this.jobService.refreshListData(this.stateGet).subscribe();
  }

  handleGetById(data: IJob) {
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }
    this.modalJobItem = data
    this.isLoading = true
  
    const isDuplicate = this.breadcrumbItems.some(crumb => crumb.idJob === data.idJob);
    if (!isDuplicate) {
      this.breadcrumbItems.push(data);
    }
        // check id truyền vào có tồn tại trong breadcrumb không
    // if (!this.breadcrumbItems.includes(this.modalJobItem)) {
    //   this.breadcrumbItems.push(this.modalJobItem);
    // }
    // console.log(this.breadcrumbItems, "giá trị của breadcum");
    this.jobService.getJobById(this.stateGet, data.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
        }

        this.listData = v.content.list
        this.totalRecord = v.content.totalRecord
        this.stateGet.currentPage = v.content.currentPage
        this.isLoading = false
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
        this.isLoading = false
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['item'] && changes['item'].currentValue.idJob > 0) {
      //  this.handleGetById()
      this.breadcrumbItems.push(this.item)
      // console.log(this.item,"item sau khi get by id");
      // console.log(this.breadcrumbItems[0],"breadcrumb tại vt 0  ");
      // console.log(this.breadcrumbItems.length,"độ dài breadcrumb khi mở modal")
      this.jobService.get().pipe(takeUntil(this.destroyed$)).subscribe(data => {
        this.listData = data.content.list
        this.totalRecord = data.content.totalRecord
        this.stateGet.currentPage = data.content.currentPage
      })
    }
  }

  handleReSelectSortType(value: number) {
    this.sortItem = value
  }

  handleSortData(e: number) {
    const data = sortCaseFilter(e)
    if (typeof (data) === 'number') {
      return
    }
    this.stateGet.sortType = data.sortType
    this.stateGet.sortData = data.sortData
    this.stateGet.currentPage = 1
    this.jobService.getJobById(this.stateGet, this.item.idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    }
    )
  }

  handleSearch() {
    this.stateGet.currentPage = 1
    this.jobService.getJobById(this.stateGet, this.item.idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handlePagination(event: number) {
    this.isLoading = true;
    this.stateGet.currentPage = event;
    this.jobService.getJobById(this.stateGet, this.item.idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    });
  }
  handleroundProgress(progressDouble: number): string {
    const roundedPercentage = Math.round(progressDouble);
    return roundedPercentage.toString();
  }
  handlePriorityFormat(priority: string) {
    return priorityFormat(priority)
  }

  handleOpenModalAddorUpdateJob(item: any, index?: number) {
    if (item) {
      // this.modalItemAddorUpdateJob.itemData = item
      // this.modalItemAddorUpdateJob.listData = this.listData
      // this.modalItemAddorUpdateJob.index = index
    } else {
      this.modalItemAddorUpdate.itemData = {
        idJob: 0,
        nameJob: '',
        idManage: 0,
        idStatus: 1,
        nameStatus: '',
        priority: '',
        createdAt: new Date,
        updatedAt: new Date
      }
      this.modalItemAddorUpdate.listData = {}
      this.modalItemAddorUpdate.index = index
    }
    this.modalJob.modalAddorUpdate = true
    console.log(this.modalJob.modalAddorUpdate)
  }

  handleCloseModalAddorUpdateModalJob() {
    this.modalJob.modalAddorUpdate = false;
  }

  onInput() {
    this.showValidate = this.nameJob.trim() === '';
  }

  handleResetState() {
    this.idManage = 0;
    this.nameJob = '';
    this.priority = 'ONE';
    this.idStatus = 1;
  }

}




