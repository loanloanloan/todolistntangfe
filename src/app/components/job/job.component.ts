import { Component, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { JobService } from '../../service/jobService/job.service';
import { Subject, takeUntil } from 'rxjs';
import { IJob, IStateGetJob } from '../../Interface/IJob';
import { limit } from '../../constantCommon/constantCommon';
import { priorityFormat } from '../../utils/priorityFormat';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrl: './job.component.css'
})
export class JobComponent {

  isVisible = false;
  isLoading = false;
  isSearch: boolean = false;
  listData: IJob[] = [];
  limit = limit;
  totalRecord: number = 0

  sortItem = 9
  
  private destroyed$ = new Subject()

  modalJob = {
    modalJobItem: false,
    modalAddorUpdate: false,
    modalDelete: false
  }

  modalItemAddorUpdate = {
    listData: {} as IJob[] | any,
    index: 0 as number | any,
    itemData: {} as IJob | any
  }


  modalDeleteItem: IJob | any = {}
  modalJobItem: IJob| any = {}
  modalItemDelete: IJob | any = {}


  constructor(private jobService: JobService, private notification: NzNotificationService) {}
  stateGet = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }

  handleGetAll() {
    this.isLoading = true
    this.jobService.refreshListData(this.stateGet).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
        }
        this.isLoading = false
      },
      error: (error) => {
        this.notification.create(
          "error", `${error.message}`, '');
        this.isLoading = false
      }
    })
  }
  ngOnInit(): void {
    this.handleGetAll()
    this.jobService.get().pipe(takeUntil(this.destroyed$)).subscribe(data => {
      this.listData = data.content.list
      this.totalRecord = data.content.totalRecord
      this.stateGet.currentPage = data.content.currentPage
    })
  }

  handleOpenModalAddorUpdate(data?: IJob, index?: number) {
    if(data) {
      this.modalItemAddorUpdate.listData = this.listData
      this.modalItemAddorUpdate.itemData = data
      this.modalItemAddorUpdate.index = index
      this.modalJob.modalAddorUpdate = true
    } else {
      this.modalItemAddorUpdate.itemData = {
        idJob: 0,
        nameJob: '',
        priority: 'ONE',
        nameStatus:'',
      }
      this.modalJob.modalAddorUpdate = true;
    }
      
  }

  handleCloseModalAddorUpdate() {
    this.modalJob.modalAddorUpdate = false;
  }

  handleOpenModalDelete(data?: IJob) {
    if (data) {
      this.modalDeleteItem = data
    } else {
      this.modalDeleteItem = {}
    }
    this.modalJob.modalDelete = true;
  }

  handleCloseModalDelete() {
    this.modalDeleteItem = undefined;
    this.modalJob.modalDelete = false;
  }

  // modal job
  handleOpenModalJob(data: IJob) {
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }
    this.modalJobItem = data
    // console.log(this.stateGet,"this.state get  job comlonent trước khi call")
    // console.log(this.modalJobItem,"this.modalJobItem get job comlonent trước khi call")

    this.jobService.getJobById(this.stateGet, this.modalJobItem.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
          this.listData = v.content.list
          this.totalRecord = v.content.totalRecord
          this.stateGet.currentPage = v.content.currentPage
        }
        this.isLoading = false
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
        this.isLoading = false
      }
    })
    this.modalJob.modalJobItem = true
    // console.log(this.modalJobItem,"this.modalJobItem")
  }

  handleCloseModalJob() {
    this.modalJob.modalJobItem = false

    this.isLoading = true
    this.modalJobItem = {
      idJob: 0,
      nameJob: '',
      idStatus: 0,
      nameStatus: '',
      priority: 'ONE',
      progress: '',
      progressDouble: 1,
      hasChild: false,
      createdAt: new Date(),
      updatedAt: new Date(),
      idManage: 0
    }
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }
    this.jobService.refreshListData(this.stateGet).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
        }
        this.isLoading = false
      },
      error: (error) => {
        this.notification.create(
          "error", `${error.message}`, '');
        this.isLoading = false
      }
    })
  }

  handleReSelectSortType(value: number) {
    this.sortItem = value;
  }

  handleSearch(event: IStateGetJob): void {
    console.log(event, "testing");
    this.isSearch = true;
    this.stateGet.currentPage = 1
    this.jobService.refreshListData(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }


  handlePagination(event: number) {
    if (!this.isSearch) {
      this.stateGet.textSearch = '';
      this.isLoading = true;
      this.stateGet.currentPage = event;

      this.jobService.refreshListData(this.stateGet).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.listData = v.content.list
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    } else {
      this.isLoading = true;
      this.stateGet.currentPage = event;

      this.jobService.refreshListData(this.stateGet).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.listData = v.content.list
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    }
  }

  handlePriorityFormat(priority: string) {
    return priorityFormat(priority)
  }


  handleroundProgress(progressDouble: number): string {
    const roundedPercentage = Math.round(progressDouble);
    return roundedPercentage.toString();
  }

  
}
