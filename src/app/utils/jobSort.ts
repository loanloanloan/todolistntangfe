import { ISortcase } from "../Interface/IJob"

export const sortCase = [
    { value: 1, label: 'Tên từ a -> z' },
    { value: 2, label: 'Tên từ z -> a' },
    { value: 3, label: 'Độ ưu tiên cao -> thấp' },
    { value: 4, label: 'Độ ưu tiên thấp -> cao' },
    { value: 5, label: 'Tiến độ ít -> nhiều' },
    { value: 6, label: 'Tiến độ nhiều -> ít' },
    { value: 7, label: 'Ngày tạo mới nhất' },
    { value: 8, label: 'Ngày tạo cũ nhất' },
    { value: 9, label: 'Ngày cập nhật mới nhất' },
    { value: 10, label: 'Ngày cập nhật cũ nhất' },
]

export const sortCaseFilter = (value: number ): ISortcase | number => {
    if (value === 1) {
        return { sortType: 'asc', sortData: 'nameJob' }
    } else if (value === 2) {
        return { sortType: 'desc', sortData: 'nameJob' }
    } else if (value === 3) {
        return { sortType: 'asc', sortData: 'priority' }
    } else if (value === 4) {
        return { sortType: 'desc', sortData: 'priority' }
    } else if (value === 5) {
        return { sortType: 'desc', sortData: 'progress' }
    } else if (value === 6) {
        return { sortType: 'asc', sortData: 'progress' }
    } else if (value === 7) {
        return { sortType: 'desc', sortData: 'createdAt' }
    } else if (value === 8) {
        return { sortType: 'asc', sortData: 'createdAt' }
    } else if (value === 9) {
        return { sortType: 'desc', sortData: 'updatedAt' }
    } else if (value === 10) {
        return { sortType: 'asc', sortData: 'updatedAt' }
    }
    return value
}