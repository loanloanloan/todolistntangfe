import { ISortcase } from "../Interface/IStatus"

export const sortCase = [
    { value: 1, label: 'Ngày tạo' },
    { value: 2, label: 'Tên từ a -> z' },
    { value: 3, label: 'Tên từ z -> a' },
]

export const sortCaseFilter = (value: number ): ISortcase | number => {
    if (value === 1) {
        return { sortType: 'ASC', sortData: 'id' }
    } else if (value === 2) {
        return { sortType: 'ASC', sortData: 'nameStatus' }
    } else if (value === 3) {
        return { sortType: 'DESC', sortData: 'nameStatus' }
    } 
    return value
}