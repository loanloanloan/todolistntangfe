import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { provideHttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsComponent } from './components/components.component';
import { JobComponent } from './components/job/job.component';
import { StatusComponent } from './components/status/status.component';
import { HeaderJobComponent } from './components/job/common-job/header-job/header-job.component';
import { ModalAddOrUpdateJobComponent } from './components/job/common-job/modal-add-or-update-job/modal-add-or-update-job.component';
import { ModalDeleteJobComponent } from './components/job/common-job/modal-delete-job/modal-delete-job.component';
import { ModalJobComponent } from './components/job/modal-job/modal-job.component';
import { ModalDeleteStatusComponent } from './components/status/common-status/modal-delete-status/modal-delete-status.component';
import { ModalAddOrUpdateStatusComponent } from './components/status/common-status/modal-add-or-update-status/modal-add-or-update-status.component';
import { HeaderStatusComponent } from './components/status/common-status/header-status/header-status.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

registerLocaleData(en);

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { PieChartOutline, BarChartOutline } from '@ant-design/icons-angular/icons';
//link
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';



registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    JobComponent,
    StatusComponent,
    HeaderJobComponent,
    ModalAddOrUpdateJobComponent,
    ModalDeleteJobComponent,
    ModalJobComponent,
    ModalDeleteStatusComponent,
    ModalAddOrUpdateStatusComponent,
    HeaderStatusComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NzButtonModule,
    NzModalModule,
    NzTableModule,
    NzInputModule,
    NzIconModule,
    NzPaginationModule,
    NzSelectModule,
    NzFormModule,
    NzLayoutModule,
    NzMenuModule,
    NzNotificationModule,
    NzImageModule,
    NzAlertModule,
    FormsModule,
    NzModalModule,
    //link
    NzBreadCrumbModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: [PieChartOutline, BarChartOutline] },
    provideAnimationsAsync(),
    provideHttpClient()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
