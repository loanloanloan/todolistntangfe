import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, ReplaySubject, tap } from 'rxjs';

import { IJob,IPayloadJob, IResponse, IStateGetJob } from '../../Interface/IJob';

import { apiMethod, baseUrl, objectApi, limit } from '../../constantCommon/constantCommon';
@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http : HttpClient) { }
  private response = new ReplaySubject<IResponse>()

  get(): Observable<IResponse> {
    return this.response.asObservable()
  }

  checkJob(idJob: number | undefined): Observable<IResponse>{
    return this.http.get<any>(
      `${baseUrl}${objectApi.job}${apiMethod.checkIdJob}/${idJob}`
    );
  }

  refreshListData(data: IStateGetJob): Observable<IResponse>{
    return this.http.get<IResponse>(`${baseUrl}${objectApi.job}${apiMethod.getAll}?textSearch=${data.textSearch}&currentPage=${data.currentPage}&sortData=${data.sortData}&sortType=${data.sortType}&limit=${limit}`)
    .pipe(tap(res => {
      this.response.next(res)
    }))
  }
  getJobById(data: IStateGetJob, idJob: number): Observable<IResponse>{
    return this.http.get<IResponse>(`${baseUrl}${objectApi.job}${apiMethod.get}?idJob=${idJob}&textSearch=${data.textSearch}&currentPage=${data.currentPage}&sortData=${data.sortData}&sortType=${data.sortType}&limit=${limit}`)
    .pipe(tap(res => {
      this.response.next(res)
    }))
  }
  delete(idJob: number, currentPage: number, idManage: number, limit: number): Observable<any>{
    return this.http.delete<any>(
      `${baseUrl}${objectApi.job}${apiMethod.delete}/${idJob}?&limit=${limit}&currentPage=${currentPage}&idManage=${idManage}`
    )
  }
  
  // add(data: IPayloadJob): Observable<IResponse> {
  //   return this.http.post<IResponse>(
  //     `${baseUrl}${objectApi.job}${apiMethod.post}?idManage=${data.idManage}&idStatus=${data.idStatus}&nameStatus=${data.statusName}&priority`)
  // }

  add(data: IPayloadJob): Observable<any> {
    return this.http.post<any>(
      `${baseUrl}${objectApi.job}${apiMethod.post}?idManage=${data.idManage}`, data
    );
  }
  update(idJob: number, idManage: number, nameJob: string, priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE', idStatus: number, nameStatus: string, currentlyPage: number, totalItem: number, listData: IJob[] , index: number, data: IStateGetJob): Observable<any> {
    const url = `${baseUrl}${objectApi.job}{apiMethod.put}/${idJob}`
    const body = {nameJob, priority, idStatus, nameStatus}
    return this.http.put<IResponse>(url, body).pipe(tap(res => {
      const updateListData: IJob[] = [...listData]
      const updateJob: IJob = {
        idJob,
        nameJob,
        idStatus: idStatus,
        priority,
        progress: updateListData[index].progress,
        progressDouble: updateListData[index].progressDouble,
        hasChild: updateListData[index].hasChild,
        createdAt: updateListData[index].createdAt,
        updatedAt: new Date,
        idManage,
        nameStatus
      };
      updateListData[index] = updateJob

      this.response.next({
        message: res.message,
        status: res.status,
        content: {
          list: updateListData,
          totalRecord: totalItem,
          currentPage: currentlyPage,
          warning: '',
        }
      })
    }))
  }
}
  
