export interface IStatus {
    idStatus: number,
    nameStatus: string,
    description: string,
    default: boolean
}

export interface IStateGetStatus {
    textSearch?: string,
    sortData?: string,
    sortType?: string,
    currentPage: number
}
export interface IResponse {
    message: string,
    status: boolean,
    content:{
        list: IStatus[],
        totalRecord: number,
        currentPage: number,    
        warning: string
    }
}
export interface ISortcase {
    sortData: string,
    sortType: string
}