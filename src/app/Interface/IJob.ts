export interface IJob {
    idJob: number,
    nameJob: string,
    idStatus: number,
    nameStatus: string,
    priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE',
    progress: string,
    progressDouble: number,
    hasChild: boolean,
    createdAt?: Date,
    updatedAt?: Date,
    idManage: number
}

export interface IStateGetJob {
    textSearch?: string,
    sortData?: string,
    sortType?: string,
    currentPage: number
}

export interface IPayloadJob {
    idJob?: number,
    nameJob: string,
    idStatus?: number,
    statusName?: string,
    idManage?: number,
    priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE'
}

export interface IResponseContent {
    // content: any
    list: IJob[],
    totalRecord: number,
    currentPage: number,
    warning: string
}
export interface IResponse {
    message: string,
    status: boolean,
    content: IResponseContent
}

export interface ISortcase {
    sortData: string,
    sortType: string
}