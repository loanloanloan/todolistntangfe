import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobComponent } from './components/job/job.component';
import { StatusComponent } from './components/status/status.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/job' },
  { path: 'job', component: JobComponent },
  { path: 'status', component: StatusComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
