export const baseUrl = `http://localhost:8080`

export const apiMethod = {
    get: '/get',
    getAll:'/getAll',
    post: '/add',
    put: '/update',
    delete: '/delete',
    back: '/back',
    link: '/link',
    checkIdStatus: '/checkIdStatus',
    checkIdJob: '/checkIdJob',
}

export const objectApi = {
    job: `/job`,
    status: `/status`
}

export const limit = 5

export const jobSortArray = [
    {value: 1, label: 'Tên từ a -> z'},
    {value: 2, label: 'Tên từ z -> a'},
    {value: 3, label: 'Độ ưu tiên từ thấp đến cao'},
    {value: 4, label: 'Độ ưu tiên từ cao đến thấp'},
    {value: 5, label: 'Tiến độ nhiều -> ít'},
    {value: 6, label: 'Tiến độ ít -> nhiều'},
    {value: 7, label: 'Ngày tạo mới nhất'},
    {value: 8, label: 'Ngày tạo cũ nhất'},
    {value: 9, label: 'Ngày cập nhật mới nhất'},
    {value: 10, label: 'Ngày cập nhật cũ nhất'},
]

